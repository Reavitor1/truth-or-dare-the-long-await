//
//  ViewController.swift
//  Truth or Dare the best
//
//  Created by christian  on 2/22/15.
//  Copyright (c) 2015 Reavitor1city. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {    
  // Mark: Adult truth button
    @IBAction func truthButton(sender: AnyObject) {
        truthDareLabel.text = adultruth.randomTruth()
    }
    
    @IBOutlet weak var truthLabel: UILabel!
    let adultruth = AdultTruth()

   //Mark:Adult Dare Button
    @IBAction func dareButton(sender: AnyObject) {
    truthDareLabel.text = adultdare.randomDare()
  }
  
  @IBOutlet weak var truthDareLabel: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
    view.backgroundColor = UIColor(red: 10/225.0, green: 50/255.0, blue: 250/255.0, alpha: 1.0)
  }
    
   let adultdare = AdultDare()
   	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 }